# Binary Tree

Application in **Nodejs** for the creation of binary trees and the choice of their closest ancestors according to two nodes.

##  Pre requirements
+ Have node installed on your desktop, in case you don't have it download here:

		https://nodejs.org/es/
		
## Installation

+ Download the source code of this project with the following link :
		
		https://gitlab.com/Jerson-JE/arbol-binario
+ Through the console, locate yourself at the root of the project:

		\arbol-binario>
	+ Within that promt execute the command **npm install** as shown in the following picture:
	
		![enter image description here](https://gitlab.com/Jerson-JE/arbol-binario/raw/development/sourceReadme/install.JPG)
		
	+ Once the project is installed execute the command in the console **node server**  as shown in the following picture:
	
		![enter image description here](https://gitlab.com/Jerson-JE/arbol-binario/raw/development/sourceReadme/deploy.JPG)
	
	+	Once I have done the previous steps the project is already deployed and ready to test as shown in the following picture: 
	
		![enter image description here](https://gitlab.com/Jerson-JE/arbol-binario/raw/development/sourceReadme/conection.JPG)


## Endpoints

The application exposes 2 endpoints:

>  **POST**
+ **/tree/** | This endpoint serves to create a binary tree, receives in the body an object with the following structure, structure explanation:
+ 
![enter image description here](https://gitlab.com/Jerson-JE/arbol-binario/raw/development/sourceReadme/estructure-binari-tree.png)

```json
{
	"branch": 2,
	"ancestor": 67,
	"node": 39
}
```

>  **GET**
+ **/tree?nodeOne=39&nodeTwo=76** This endpoint serves to query the ancestor closest between two nodes, the parameters it handles are "query params", that is, they receive them through the URL


## Examples use API

>  **POST**
+ **/tree/** 

```json
Body:  
{
	"branch":1,
	"ancestor": 0,
	"node": 67
}
```

>  **GET**
+ **/tree?nodeOne=29&nodeTwo=87**

## Test Binary Tree  Objects 

![enter image description here](https://gitlab.com/Jerson-JE/arbol-binario/raw/development/sourceReadme/treeTest.PNG)

```json
{"branch":  1, "ancestor":  0, "node":  67}

{"branch":  2, "ancestor":  67,"node":  39}

{"branch":  2, "ancestor":  67,"node":  76}

{"branch":  3, "ancestor":  39,"node":  44}

{"branch":  3, "ancestor":  39,"node":  28}

{"branch":  3, "ancestor":  76,"node":  85}

{"branch":  3, "ancestor":  76,"node":  74}

{"branch":  4, "ancestor":  28,"node":  29}

{"branch":  4, "ancestor":  85,"node":  83}

{"branch":  4, "ancestor":  85,"node":  87}
```
