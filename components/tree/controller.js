/**
 * ARCHIVO CONTROLADOR DE DATOS ARBOL
 */

 /** Importación archivo store comunicaciòn base de datos */
const store = require('./store');
/** Importación  funcionalidad del antecesor*/
const filter = require('./filterAncestor');

/**
 * Funcion para la creación de nodos
 * @param {*} branch 
 * @param {*} ancestor 
 * @param {*} node 
 */
function addNode (branch, ancestor, node) {
  // Retorno y declaración de promesa para el guardado de los nodos
  return new Promise ((resolve, reject) => {
    // validación objeto nodo
    if (branch === undefined|| ancestor === undefined || node  === undefined) {
      console.error(`[messageController] No hay branch - ancestor - node`);
      reject('los datos son incorrectos');
      return false
      
    }
    // creacion objeto fullNode para enviar hacia store
    const fullNode = {
      branch: branch,
      ancestor: ancestor,
      node: node,
      date: new Date()
    };
    // Envio de objeto para creación de nodo
    store.add(fullNode);
    // Retorno promesa
    resolve(fullNode);
  });
}

/**
 * Funcion para obtener el antecesor de dos nodos
 * @param {*} nodeOne 
 * @param {*} nodeTwo 
 */
function getAncestor (nodeOne, nodeTwo) {
  return new Promise((resolve, reject) => {
    let result = store.list()
    .then((response) => {
      // Envio de infomración del arbol para obtener el antecesor de los nodos requeridos
      return result = filter.filterAncetor(nodeOne, nodeTwo, response);
    })
     // Retorno promesa
    resolve(result);
  })
}

/** Exportación de funciones*/
module.exports = {
  addNode,
  getAncestor,
}