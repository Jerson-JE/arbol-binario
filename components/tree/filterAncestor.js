/**
 * FUNCIONALIDAD OBTENER ANTECESOR
*/

/**
 * Funcion que obtiene el antecesor
 * @param {*} nodeOne 
 * @param {*} nodeTwo 
 * @param {*} listNodes 
 */
function LowestCommonAncestor (nodeOne, nodeTwo, listNodes) {
  // Declaracion e inicilización de obejtos temporales
  let listNodeOne = [],
      listNodeTwo = []

  let objectNodeOne, objectNodeTwo

  // Llamado a [returnNode] para obtener el objeto completo de los nodos indicados por el servicio
  objectNodeOne = returnNode(nodeOne, listNodes, false, false);
  objectNodeTwo = returnNode(nodeTwo, listNodes, false, false);

  // Llamado a [returnNode] para obtener el antecesor e insercion en lista temporal <listNodeOne>
  listNodeOne.push(returnNode(objectNodeOne, listNodes, false, true));
  listNodeTwo.push(returnNode(objectNodeTwo, listNodes, false, true));

  // Llamado a [returnListAncestor] para obtener el listado completo de los antecesores de cada nodo
  listNodeOne = returnListAncestor(listNodeOne, listNodes);
  listNodeTwo = returnListAncestor(listNodeTwo, listNodes);

  // Declaracion de lsita <listFathers> temporal para guardar antecesores
  const listFathers = [];

  // Identificación de antecesores comúnes entre los nodos
  listNodeOne.forEach(elementNodeOne => {
    listNodeTwo.forEach(elementNodeTwo => {
      if (elementNodeOne.node === elementNodeTwo.node) {
        listFathers.push(elementNodeOne)
      }
    });
  });

  // Declaracion de lista <listNumbers> temporal para guardar los niveles(branch) de los antecesores identificados
  const listNumbers = [];
  // Insercion en lista temporal de los niveles(branch)
  listFathers.forEach(element => {
    listNumbers.push(element.branch);
  });

  // Declaracion respuesta antecesor común mas cercano
  let ancestor;
  // Filtrado de el antecesor mas cercano entre los nodos
  ancestor = listFathers.filter(element => {
    return element.branch === Math.max.apply(null, listNumbers);
  });

  // Retorno antecesor
  return ancestor;
};


/**
 * Funcion para devolver el objeto del nodo indicado, ya sea un antecesor, nodo correspondiente, o ultimo nodo dentro de una lista de nodos
 * @param {*} node 
 * @param {*} listNodes 
 * @param {*} lastest 
 * @param {*} ancestor 
 */
function returnNode (node, listNodes, lastest, ancestor) {
  // Declaración respuesta posición
  let posicion;
  // Validación ultimo nodo
  if (lastest === true) {
    let copyList;
    copyList = JSON.parse(listNodes);
    posicion = copyList.pop();
    return posicion;
  } else {
    // Validacion para saber si lo que se busca es el nodo correspondiente o el antecesor
    if (ancestor === true) {
      filter = (node.ancestor === 0 ? node.node : node.ancestor)
    } else {
      filter = node;
    }
    // Filtro encontrar posición indicada del nodo
    posicion = listNodes.filter(element => {
      return element.node === parseInt(filter);
    });
  }
  // Retorno objeto nodo indicado
  return posicion[0];
};

/**
 * Funcion que devuelve la lista todos los antecesores de un nodo especifico
 * @param {*} listAncestors 
 * @param {*} listNodes 
 */
function returnListAncestor (listAncestors, listNodes) {
  // Declaracion lista <contador> temporal para la obtencion de los nodos
  let contador = [];
  // Se obtiene el ultimo nodo de la lista de antecesores
  contador.push(returnNode(null, JSON.stringify(listAncestors), true, false));

  // Se deternima loop para capturar todos sus antecesores
  while (contador.length > 0) {
    let respuesta;
    // Verificacion de exitencia del ultimo nodo antecesor
    respuesta = returnNode(contador[0].ancestor, listNodes, false, false);
    if (respuesta !== undefined) {
      listAncestors.push(respuesta);
      contador = [];
      // Insercion en lista de retorno <listAncestors> 
      contador.push(returnNode(null, JSON.stringify(listAncestors), true, false));
    } else {
      contador = [];
    }
  }
  // Retorno listado de antecesores
  return listAncestors
};

/** Exportación de funciones*/
module.exports = {
  filterAncetor: LowestCommonAncestor,
}