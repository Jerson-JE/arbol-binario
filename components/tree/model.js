/**
 * ARCHIVO CREACIÓN MODELO PARA LA BASE DE DATOS
 */
/** Importación mongoose contexto [mongodb] */
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/** Declaracion mySchema para la creacion de la estructura del modelo*/
const mySchema = new Schema({
  branch: Number,
  ancestor: Number,
  node: Number,
  date: Date
})

// Creación del schema Nodes que se enviara al servidor de base de datos
const model = mongoose.model('Nodes', mySchema);

/** Exportación de funciones*/
module.exports = model;