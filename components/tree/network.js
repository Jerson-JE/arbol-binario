/**
 * ARCHIVO PARAMETRIZACIÓN ENDPOINTS, DONDE RESUELVEN LAS PETICIONES
 */
/** Importación de  [express]*/
const express = require('express');
/** Importacion de controladores y modulo de respuesta de la aplicación */
const reply = require('../../network/response');
const controller = require('./controller')

/** Inicializacion de Router para el contexto */
const router = express.Router();

/**
 * METODO: GET
 * Se recepciona los nodos a los que se requiere encontrar el antecesor mas cercano
*/
router.get('/', function (request, response) {
  // Construcción objeto filtro conjutno a los parametros de url 
  const filtersNodes = {
    one: request.query.nodeOne,
    two: request.query.nodeTwo
  }
  // Llamado al controlador [getAncestor] para obtener ancestro
  controller.getAncestor(filtersNodes.one, filtersNodes.two)
    .then((ancestor) => {
      // Llamado modulo respuesta [success]
      reply.success(request, response, ancestor, 200);
    })
    .catch(e => {
      // Llamado modulo respuesta [error]
      reply.error(request, response, 'Unexpected error', 500, e);
    });
})

/**
 * METODO: POST
 * Se recepciona el cuerpo del nodo a crear
 */
router.post('/', function (request, response) {
  // Llamado al controlador [addNode]
  controller.addNode(request.body.branch, request.body.ancestor, request.body.node)
    .then((fullNode) => {
      // Llamado modulo respuesta [success]
      reply.success(request, response, fullNode, 200);
    })
    .catch(e => {
      // Llamado modulo respuesta [error]
      reply.error(request, response, 'Informacion invalida', 400, 'Error en el controlador')
    });
});

/** Exportación de funciones*/
module.exports = router;