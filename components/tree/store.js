/**
 * ARCHIVO CONEXIÓn BASE DE DATOS
 */
/** Importación modelo y acceso al contexto base de datos */
const Model = require('./model');

/**
 * Funcion para crear nodos
 * @param {*} node 
 */
function addNode(node) {
  const myNode = new Model(node)
  myNode.save();
}

/**
 * Funcion para obtener el arbol binario
 */
async function getNode() {
  const node = await Model.find();
  return node;
}

/** Exportación de funciones*/
module.exports = {
  add: addNode,
  list: getNode,
}