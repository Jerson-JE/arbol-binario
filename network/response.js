/**
 * ARCHIVO PARAMETRIZACIÓN DE RESPUESTA LOS SERVICIOS
 */

 /** Exportacion funcion success respuestas correctas */
exports.success = function (request, response, message, status) {
  response.status(status || 200).send({
    data: {
      error: '',
      body: message
    }
  });
}

/** Exportacion funcion error respuestas incorrectas */
exports.error = function (request, response, message, status, details) {
  console.error(`[response error] ${details}`)
  response.status(status || 500).send({
    data: {
      error: message,
      body: ''
    }
  });
}