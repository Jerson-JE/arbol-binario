/**
 * ARCHIVO PARAMETRIZACIÓN DE RUTAS, REDIRECCIONAMIENTO DE RUTAS
 */

/** Importación [express]*/
const express = require('express')
/** Importación del archivo donde se expone los endpoints */
const tree = require('../components/tree/network')

/**  Funcion para la vinculacion de la ruta /tree con los endpoints*/
const routes = function (server) {
  server.use('/tree', tree);
}

/** Exportación de funciones*/
module.exports = routes