/**
 * ARCHIVO DE CONFIGURACIÓN DE LA APLICACIÓN
 */

/** Se importan las librerias de [express] y [body-parser]*/
const express = require('express');
const bodyParser = require('body-parser');

/** Se importa la conexión hacia la base de datos */
const condexionDb = require('./db');

/** Se importa el archivo de vinculación con la aplicación por medio de las rutas*/
const router = require('./network/routes');

/** Se ejecuta la conexión hacia la base de datos */
condexionDb('mongodb+srv://db_user_prueba:n4ZyDBpoqJWC6XZ5@cluster0-ve7ru.mongodb.net/prueba_db');

/** Inicialización de la aplicación apartir de [express] */
const app = express();

/** Se establece el metodo de comunicación de las peticiones con la aplicación */
app.use(bodyParser.json());

/** Se enlaza la ejecución de la aplicación por medio de las rutas */
router(app);

/** Despliegue de la aplicacion atraves del puerto 3000 */
app.listen(3000);
console.log('[conecction] The server is connected and expose the port: http://localhost:3000');